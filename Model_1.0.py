
# coding: utf-8

# In[966]:


from pandas.tools.plotting import scatter_matrix
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, f1_score, confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, LabelEncoder, LabelBinarizer
from scipy.stats.stats import pearsonr

import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats, integrate

from numpy.random import seed
from numpy.random import randn
from scipy.stats import shapiro
from scipy.stats import normaltest
from sklearn.metrics import recall_score
from imblearn.over_sampling import SMOTE

from pandas import Series
from sklearn.preprocessing import MinMaxScaler


# In[967]:


df = pd.read_csv('ActiveUsersEntri.csv')


# In[968]:


#Renaming Columns

df.columns = ['id','ques_attempt','test_attempt','subexam_attempt','mockexam_attempt','no_subplatforms','no_paidplatforms','inviteflag','couponflag','paymentfailflag','converted']


# In[969]:


df.shape


# In[970]:


ques_attempt = df['ques_attempt'].values
ques_attempt = values.reshape((len(values), 1))
#test_attempt = df['test_attempt'].values
#test_attempt = values.reshape((len(values), 1))
#subexam_attempt = df['subexam_attempt'].values
#subexam_attempt = values.reshape((len(values), 1))
#mockexam_attempt = df['mockexam_attempt'].values
#mockexam_attempt = values.reshape((len(values), 1))
#no_subplatforms = df['no_subplatforms'].values
#no_subplatforms = values.reshape((len(values), 1))

scaler = MinMaxScaler(feature_range=(0, 1))
scaler = scaler.fit(values)
df['ques_attempt'] = scaler.transform(ques_attempt)
#df['test_attempt'] = scaler.transform(test_attempt)
#df['subexam_attempt'] = scaler.transform(subexam_attempt)
#df['mockexam_attempt'] = scaler.transform(mockexam_attempt)
#df['no_subplatforms'] = scaler.transform(no_subplatforms)


# In[673]:


df.head()


# In[674]:


#df[['paymentfailflag','inviteflag']] = df[['paymentfailflag','inviteflag']].astype(bool) #converting flags to boolean
df= df.drop(['id','couponflag','no_paidplatforms'],axis=1)


# In[675]:


df_target = df['converted']
df_features = df.drop('converted', axis=1)


# In[676]:


print(df_features.shape)
print(df_target.shape)


# In[710]:


#Split to test and train

features_train,features_test,target_train,target_test = train_test_split(df_features, df_target,
                                                  test_size = .3,
                                                  random_state=12)


# In[711]:



print("Target test")
print(target_test.shape)
print("Features test")
print(features_test.shape)
print("Features train")
print(features_train.shape)
print("Target train")
print(target_train.shape)
print("Target train converted proportion")
print(sum(target_train)/len(target_train))
print("Target test converted proportion")
print(sum(target_test)/len(target_test))


# In[712]:


sns.lmplot(x="ques_attempt", y="test_attempt", data=df, col = 'converted')


# In[945]:


sm = SMOTE(random_state=0,ratio = .025,k_neighbors =9 )
    
#help(SMOTE)


# In[946]:


features_train_res, target_train_res = sm.fit_sample(features_train, target_train)


# In[947]:


aftersmote = pd.DataFrame(features_train_res,index=features_train_res[:,0])

aftersmote['converted'] = target_train_res

aftersmote.columns = ['ques','test','ss','sss','ssss','we','ghh','convertedd']


# In[948]:


print(sns.lmplot(x="ques_attempt", y="test_attempt", data=df, col = 'converted'))
print(sns.lmplot(x="ques", y="test", data=aftersmote,col="convertedd"))


# In[949]:


features_train_res.shape


# In[950]:


sum(target_train_res)


# In[951]:


print("new proportion")
print(sum(target_train_res)/len(target_train_res)*100)


# In[952]:


target_train.shape


# In[953]:


# Random Forest Trainer
#forest=RandomForestClassifier(n_estimators=100, max_depth=5,random_state=15)
forest=RandomForestClassifier(n_estimators=25, random_state=12)
forest.fit(features_train_res, target_train_res)
print("Training score: {:.4f}".format(forest.score(features_train_res, target_train_res)))


# In[954]:


pred = forest.predict(features_test)


# In[955]:


confusion_matrix(target_test,pred)


# In[956]:


from sklearn.metrics import precision_score
print ('Accuracy:', accuracy_score(target_test, pred))
print ('F1 score:', f1_score(target_test, pred))
print ('Recall:', recall_score(target_test, pred))
print ('Precision:', precision_score(target_test, pred))
from sklearn.metrics import roc_curve, auc

false_positive_rate, true_positive_rate, thresholds = roc_curve(target_test, pred)
roc_auc = auc(false_positive_rate, true_positive_rate)
print ('ROC:', roc_auc)


# In[957]:


#Logistic Regression

from sklearn.linear_model import LogisticRegression
logreg = LogisticRegression()
logreg.fit(features_train_res, target_train_res)
predlog = logreg.predict(features_test)
print("Training score log: {:.4f}".format(logreg.score(features_train_res, target_train_res)))


# In[958]:


confusion_matrix(target_test,predlog)


# In[959]:


from sklearn.metrics import roc_curve, auc

from sklearn.metrics import precision_score
print ('Accuracy log:', accuracy_score(target_test, predlog))
print ('F1 score log:', f1_score(target_test, predlog))
print ('Recall log:', recall_score(target_test, predlog))
print ('Precision log:', precision_score(target_test, predlog))

false_positive_rate, true_positive_rate, thresholds = roc_curve(target_test, predlog)
roc_auc = auc(false_positive_rate, true_positive_rate)
print ('ROC:', roc_auc)


# In[960]:


import matplotlib.pyplot as plt
plt.title('Receiver Operating Characteristic')
plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
plt.legend(loc='lower right')
plt.plot([0,1],[0,1],'r--')
plt.xlim([-0.1,1.2])
plt.ylim([-0.1,1.2])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')


# In[961]:


import pandas as pd
feature_importances = pd.DataFrame(data=forest.feature_importances_,index = features_train.columns,columns=['importance'])
feature_importances1 = pd.DataFrame(data=forest.feature_importances_,columns=['importance forest'])
feature_importances1['variables'] = features_train.columns

feature_importances1


# In[962]:


print(logreg.coef_)


# In[971]:


predtotal = forest.predict(features_train_res)


# In[972]:


confusion_matrix(target_train_res,predtotal)

